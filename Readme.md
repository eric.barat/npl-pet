# NPL-PET

Nonparametric Posterior Learning for Emission Tomography using Multimodal Data

## Description

This repository contains sample code corresponding to the preprint *Nonparametric Posterior Learning for Emission Tomography with Multimodal Data* [(HAL-preprint)](https://hal.archives-ouvertes.fr/hal-03305272v4)

## Getting Started

### Dependencies

* Describe any prerequisites, libraries,...
* Describe conda or pip setup

### Installing

* How/where to download your program
* Any modifications needed to be made to files/folders

### Executing program

* How to run the program
* Step-by-step bullets

## Help

Any advise for common problems or issues.

## Authors

Contributors names and contact info

* Fedor Goncharov  [@Fedor](mailto:fedor.goncharov.ol@gmail.com)
* Eric Barat [@Eric](mailto:eric.barat@cea.fr)
* Thomas Dautremer [@Thomas](mailto:thomas.dautremer@cea.fr)

## Version History

* 0.1
    * Initial Release

## License

This project is licensed under the [NAME HERE] License - see the LICENSE.md file for details

