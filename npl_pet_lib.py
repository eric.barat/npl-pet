#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
# import math
from math import sqrt, log, exp, fabs, floor, ceil, cos, sin, atan2, lgamma, gamma, tanh, factorial, pi, nan, log1p
from random import random, shuffle
from numba import jit, njit, float64, int64, int32, void, boolean, optional, prange, vectorize
from numba.extending import get_cython_function_address
from numba.types import List
import ctypes
import time
from segmentation_lib import ravel_ind


###############################################################################

addr = get_cython_function_address("scipy.special.cython_special", "gammainc")
functype = ctypes.CFUNCTYPE(ctypes.c_double, ctypes.c_double, ctypes.c_double)
gammainc_fn = functype(addr)

@vectorize('float64(float64, float64)')
def vec_gammainc(x, y):
    return gammainc_fn(x, y)

@njit
def gammainc_nb(x, y):
    return vec_gammainc(x, y)

###############################################################################

addr = get_cython_function_address("scipy.special.cython_special", "gdtr")
functype = ctypes.CFUNCTYPE(ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double)
gdtr_fn = functype(addr)

@vectorize('float64(float64, float64, float64)')
def vec_gdtr(a, b, x):
    return gdtr_fn(b, a, x)

@njit
def gdtr_nb(a, b, x):
    return vec_gdtr(a, b, x)

###############################################################################

addr = get_cython_function_address("scipy.special.cython_special", "gdtrix")
functype = ctypes.CFUNCTYPE(ctypes.c_double, ctypes.c_double, ctypes.c_double, ctypes.c_double)
gdtrix_fn = functype(addr)

@vectorize('float64(float64, float64, float64)')
def vec_gdtrix(a, b, p):
    return gdtrix_fn(b, a, p)

@njit
def gdtrix_nb(a, b, p):
    return vec_gdtrix(a, b, p)

###############################################################################

@njit(float64(float64, float64, float64), cache=True)
def log_pdf_gamma(x,a,mu):
    return a*(log(a)-log(mu)) - lgamma(a) + (a-1)*log(x) - a/mu*x

# ###############################################################################

# addr = get_cython_function_address("scipy.special.cython_special", "psi")
# functype = ctypes.CFUNCTYPE(ctypes.c_double, ctypes.c_double)
# psi_fn = functype(addr)

# @vectorize('float64(float64)')
# def vec_psi(x):
#     return psi_fn(x)

# @njit
# def psi(x):
#     return vec_psi(x)

# ###############################################################################

# addr = get_cython_function_address("scipy.special.cython_special", "polygamma")
# functype = ctypes.CFUNCTYPE(ctypes.c_double, ctypes.c_int, ctypes.c_double)
# polygamma_fn = functype(addr)

# @vectorize('float64(int64, float64)')
# def vec_polygamma(n, x):
#     return polygamma_fn(n, x)

# @njit
# def polygamma_nb(n, x):
#     return vec_polygamma(n, x)

# @vectorize('float64(float64)')
# def vec_trigamma(x):
#     return polygamma_fn(1, x)

# @njit
# def trigamma(x):
#     return vec_trigamma(x)

# ###############################################################################

@njit(float64(float64), cache=True)
def psi(x):
    # *************************************************************************
    #  Check the input.
    #
    if (x <= 0.0):  # uncomment for x >0 restriction
        value = nan
        print('psi x <= 0')
        return value
#
#  Initialize.
#
    value = 0.0
#
#  Use approximation for small argument.
#
    if (x <= 0.000001):
        euler_mascheroni = 0.57721566490153286060
        value = - euler_mascheroni - 1.0 / x + 1.6449340668482264365 * x
        return value
#
#  Reduce to DIGAMA(X + N).
#
    while (x < 8.5):
        value = value - 1.0 / x
        x = x + 1.0
#
#  Use Stirling's (actually de Moivre's) expansion.
#
    r = 1.0 / x
    value = value + log(x) - 0.5 * r
    r = r * r
    value = value \
        - r * (1.0 / 12.0
               - r * (1.0 / 120.0
                      - r * (1.0 / 252.0
                             - r * (1.0 / 240.0
                                    - r * (1.0 / 132.0)))))

    return value

@njit(float64(float64), cache=True)
def psi1(x):
    small = 0.0001
    large = 5.0

    # bernoulli numbers
    b2 = 1.0 / 6.0
    b4 = -1.0 / 30.0
    b6 = 1.0 / 42.0
    b8 = -1.0 / 30.0

    if (x <= 0.0):
        value = nan
        print('psi1 x <= 0')
        return value

    # small value approximation if x <= small.
    if (x <= small):
        return 1.0 / (x * x)

    #use recurrence relation until x >= large
    z = x
    value = 0.0
    while (z < large):
        value += 1.0 / (z * z)
        z += 1.0
    # asymptotic expansion as a Laurent series if x >= large
    y = 1.0 / (z * z)
    value += 0.5 * y + (1.0 + y * (b2 + y * (b4 + y * (b6 + y * b8)))) / z
    return value

###############################################################################

@njit(float64(float64[::1], float64), cache=True)
def gamma_shape_fit(lambda_, xi):
    n = len(lambda_)
    s1 = np.mean(np.log(lambda_))
    s2 = log(np.mean(lambda_))
    # # print(len(np.nonzero(lambda_)[0]), len(lambda_))
    s = s2 - s1
    a = 0.5/s
    # if a < 1.:
    #     a = 1.
    # if a > 5.:
    #     a = 5.
    for _ in range(10):
        aold = a
        # print(a,s)
        ainv= 1./a
        g = log(a)-psi(a)-s +((xi-1)*ainv-xi)/float(n)
        h = ainv-psi1(a)-(xi-1)/float(n)*ainv**2
        # print(a, s1, s2, g, h)
        a = 1./(ainv+(g)/(a**2*h)) #
        # a = max(a,1.) #1./(ainv+g/(a**2*h)) #
        if fabs(a-aold)<1e-8:
            break
    return a

###############################################################################

@njit(float64[::1](float64[::1], float64[::1], float64, float64, float64), cache=True)
def gamma_shape_dist_fit(mlamb, llamb, mu, a0, b0):
    n = len(mlamb)
    R = np.sum(llamb)
    S = np.sum(mlamb)
    T = S/mu-R+n*log(mu)-n
    A = a0 + .5*n
    B = b0 + T
    for _ in range(10):
        a = A/B
        A = a0 - n*a + n*a**2*psi1(a)
        B = b0 + (A-a0)/a - n*log(a) + n*psi(a) + T
        if fabs(a*B/A-1.)<1e-8:
            break
    return np.array([A,B], dtype = np.float64)
    
###############################################################################

@njit(float64[::1](float64[::1], float64), cache=True)
def mode_mDir(alpha, eps):
    K = len(alpha)
    s = np.empty(K, dtype = np.float64)
    w = np.empty(K, dtype = np.float64)
    order = np.argsort(alpha)
    s[K-1] = alpha[order[K-1]] - 1.
    for k in range(K-2, -1, -1):
        s[k] = s[k+1] + alpha[order[k]] - 1.
    t = 0
    for k in range(K):
        ki = order[k]
        w[ki] = (alpha[ki]-1.)/s[t]*(1.-eps*t)
        if w[ki] < eps:
            w[ki] = eps
            t += 1    
    return w
###############################################################################

@njit(float64(float64, float64, float64), cache=True)
def gpdf(a, b, x):
    return exp(a*log(b)-lgamma(a) +(a-1)*log(x) - b*x)

###############################################################################

########################################################################

@njit(int32(float64[:], float64[:], int32[::1], float64[::1], float64[::1]), cache=True)
def siddon_size(p1, p2, dims, ori, delta):
    d = len(ori)
    eps = 1.e-12
    p1+=eps#*(np.random.random(d)-.5)
    p2+=eps#*(n
    def alpha(ind):
        return (ori+ind*delta-p1)/(p2-p1)
    def phi(a):
        return (p1+a*(p2-p1)-ori)/delta
    n = dims+1
    a0 = alpha(np.zeros(d, dtype = np.int32))
    a1 = alpha(dims)
    amin = np.array([min(a0[k], a1[k]) for k in range(d)], dtype = np.float64)
    amax = np.array([max(a0[k], a1[k]) for k in range(d)], dtype = np.float64)
    alphamin = max(amin)
    alphamax = min(amax)
    # print(alphamin, alphamax)
    if alphamin>alphamax:
        return 0
    phimin = phi(alphamin)
    phimax = phi(alphamax)
    indmin = np.empty(d, dtype = np.int32)
    indmax = np.empty(d, dtype = np.int32)
    # idx = np.empty(d, dtype = np.int32)
    for k in range(d):
        if p1[k] < p2[k]:
            indmin[k] = 1 if alphamin == amin[k] else ceil(phimin[k])
            indmax[k] = n[k]-1 if alphamax == amax[k] else floor(phimax[k])
            # idx[k] = indmin[k]
        else:
            indmin[k] = 0 if alphamax == amax[k] else ceil(phimax[k])  
            indmax[k] = n[k]-2  if alphamin == amin[k] else floor(phimin[k])
            # idx[k] = indmax[k]
    # print(indmin, indmax)
    # alp = alpha(idx) 
    Np = np.sum(indmax - indmin + 1)   
    # Np -= np.sum(alp==alphamin)
    return Np

@njit(float64[:, ::1](float64[:], float64[:], int32[::1], float64[::1], float64[::1]), cache=True)
def siddon(p1, p2, dims, ori, delta):
    d = len(ori)
    eps = 1.e-12
    p1+=eps#*(np.random.random(d)-.5)
    p2+=eps#*(np.random.random(d)-.5)
    def alpha(ind):
        return (ori+ind*delta-p1)/(p2-p1)
    def phi(a):
        return (p1+a*(p2-p1)-ori)/delta
    n = dims+1
    a0 = alpha(np.zeros(d, dtype = np.int32))
    a1 = alpha(dims)
    amin = np.array([min(a0[k], a1[k]) for k in range(d)], dtype = np.float64)
    amax = np.array([max(a0[k], a1[k]) for k in range(d)], dtype = np.float64)
    alphamin = max(amin)
    alphamax = min(amax)
    # print(alphamin, alphamax)
    if alphamin>alphamax:
        return np.empty((0,2), dtype = np.float64)
    phimin = phi(alphamin)
    phimax = phi(alphamax)
    indmin = np.empty(d, dtype = np.int32)
    indmax = np.empty(d, dtype = np.int32)
    idx = np.empty(d, dtype = np.int32)
    for k in range(d):
        if p1[k] < p2[k]:
            indmin[k] = 1 if alphamin == amin[k] else ceil(phimin[k])
            indmax[k] = n[k]-1 if alphamax == amax[k] else floor(phimax[k])
            idx[k] = indmin[k]
        else:
            indmin[k] = 0 if alphamax == amax[k] else ceil(phimax[k])  
            indmax[k] = n[k]-2  if alphamin == amin[k] else floor(phimin[k])
            idx[k] =  indmax[k]
    alp = alpha(idx)   
    # print(indmin, indmax)
    Np = np.sum(indmax - indmin + 1)   
    # Np -= np.sum(alp==alphamin)
    au = delta/np.fabs(p2-p1)
    # alp += (alp==alphamin)*au    
    lor = np.empty((Np, 2), dtype = np.float64)
    ind = np.floor(phi(.5*(min(alp)+alphamin))).astype(np.int32)
    dc = sqrt(np.sum((p2-p1)**2))
    iu = 2*(p1<p2).astype(np.int32)-1
    # d12 = 0.
    alpc = alphamin
    for i in range(Np):
        lor[i, 0] = ravel_ind(ind, dims)    
        u = np.argmin(alp)
        lor[i, 1] = (alp[u]-alpc)*dc
        alpc = alp[u]
        alp[u] += au[u]      
        ind[u] += iu[u]    
    return lor

###############################################################################

@njit(float64[:, ::1](float64[:,:], int32[::1], float64[::1], float64[::1], int32[::1]), cache=True)
def inner_system_matrix(pc, dims, ori, delta, idxlor):
    nc = len(pc)
    n = np.prod(dims)
    s = 0
    for k1 in range(nc):
        for k2 in range(k1+1, nc):
            s += siddon_size(pc[k1], pc[k2], dims, ori, delta)
    llor = 0
    t = 0
    idxlor[0] = 0
    hlor = np.empty((s, 2), dtype = np.float64)
    A = np.zeros(n, dtype = np.float64)
    for k1 in range(nc):
        for k2 in range(k1+1, nc):
            lor = siddon(pc[k1], pc[k2], dims, ori, delta)
            for l in range(len(lor)):
                A[int(lor[l,0])] += lor[l,1]
            llor += len(lor)
            t += 1
            idxlor[t] = llor
            hlor[idxlor[t-1] : idxlor[t]] = lor[:]
    for i in range(t):
        hlor[idxlor[i]:idxlor[i+1], 1] /= A[hlor[idxlor[i]:idxlor[i+1], 0].astype(np.int32)]     
    print('tot. syst.', llor)
    return hlor

# @jit(cache=True)
def build_system_matrix(pc, dims, ori, delta):
    l = len(pc)
    idxsys = np.empty((l*(l-1))//2+1, dtype = np.int32)
    hlor = inner_system_matrix(pc, dims, ori, delta, idxsys)
    jsys = hlor[:, 0].astype(np.int32)
    asys = hlor[:, 1].astype(np.float64)
    return idxsys, jsys, asys
#
@njit(float64[:,::1](int64, float64[:], boolean), cache=True)
def inner_coord_physical_lor(nc, hsino, smooth):
    # nc = len(pc)
    coord = np.empty((nc*(nc-1)//2,2), dtype = np.int32)
    i=0
    for k1 in range(nc):
        for k2 in range(k1+1, nc):
            phi = (k1+k2+nc//2)%nc
            delta = ((k2-k1) if (k1+k2<nc//2 or k1+k2>=3*nc//2) else (k1-k2))%nc
            coord[i,0]=phi
            coord[i,1]=delta
            i+=1
    ind_phi = coord.T[0]
    ind_delta = coord.T[1]
    phys_sino = np.zeros((nc,nc), dtype = np.float64)
    for i in range(len(hsino)):
        phys_sino[ind_phi[i],ind_delta[i]] = hsino[i] #gamma_[i]#
    #smoothing
    if smooth:
        phys_sino[:,:-1] += phys_sino[:,1:]
        first_row = phys_sino[0,:].copy()
        phys_sino[:-1,:] += phys_sino[1:,:]
        phys_sino[-1,:] += first_row#
        phys_sino[:,:-1] *=.25
        phys_sino[:,-1] *=.5
    #
    return phys_sino

def coord_physical_lor(pc, hsino, smooth = True):
    nc = len(pc)
    R = np.sqrt(np.sum(pc[0]**2))
    phys_sino = inner_coord_physical_lor(nc, hsino.astype(np.float64), smooth)
    u = (np.arange(phys_sino.shape[1]+1))*np.pi/nc
    delta_sino = R*np.cos(u)*np.sign(np.sin(u))
    phi_sino = np.linspace(-np.pi*.5,np.pi*.5,phys_sino.shape[0]+1)
    return phys_sino, phi_sino, delta_sino
#
@njit(float64[:,::1](int64, int64,), cache=True)
def blob2d(k,d):
    blob = np.ones((k,k), dtype=np.float64)
    for r in range(d):
        lb = (r+1)*(k-1)+k
        blob_=np.empty((lb,lb), dtype=np.float64)
        for u in range(lb):
            for v in range(lb):
                blob_[u,v] = np.sum(blob[max(0,u-k+1):u+1, max(0,v-k+1):v+1])
        blob = blob_
    return blob/k**(2*d)
#
def map_frame(shape, idxframe, lorframe, sysframe, w):
    L = shape[0]*shape[1]
    lambda_ = np.zeros(L,dtype=np.float64)
    K = len(idxframe)-1
    for i in range(K):
        lambda_[lorframe[idxframe[i]:idxframe[i+1]]]+= w[i]*sysframe[idxframe[i]:idxframe[i+1]]
    return lambda_.reshape(shape)
#
@njit((int64, int64, float64[:,::1]), cache=True)
def build_blob_frame(k, d, mask):
    n,m = mask.shape
    t = blob2d(k,d)
    size = t.shape[0]
    shifti = round(d*(k-1)*.5)+(n%k)//2
    shiftj = round(d*(k-1)*.5)+(m%k)//2#(m-k*(m//k))*.5)#+(m%k)//2
    frame = []
    ind=0
    # y = np.zeros((n,n), dtype=np.float64)
    x = np.empty((n,m), dtype=np.float64)
    for i in range(-shifti,n-shifti,k):
        for j in range(-shiftj,m-shiftj,k):
            x[:,:] = 0
            x[max(i,0):min(n,i+size),max(j,0):min(m,j+size)] = t[max(0,-i):min(size,n-i),max(0,-j):min(size,m-j)]
            x*=mask 
            index = np.nonzero(x.ravel())[0]
            values = x.ravel()[index]  
            if len(index)>0:  
                frame.append(np.vstack((index,values)))
            # print(ind,len(index),len(frame))
            ind+=1
    return frame
#
def build_segment_frame(z, mask):
    frame = []
    K = len(np.unique(z))+1
    for i in range(K):
        index = np.nonzero(((z == i)*mask).ravel())[0]
        if len(index)>0:  
            frame.append(np.vstack((index,np.ones(len(index), dtype=np.float64))))        
    return frame

@njit(float64[:, ::1](int32[::1], int32[::1], float64[::1], int32[::1], int32[::1]), cache=True)
def inner_segment_system_matrix(idxsys, lor, system, z, idxsys_z):
    M = len(idxsys)-1
    S = len(np.unique(z))
    hlor_z = np.zeros((len(lor),2), dtype = np.float64)
    sumlor = 0
    ais = np.zeros(S, dtype = np.float64)
    for i in range(M):
        ais[:] = 0.
        for jdx in range(idxsys[i], idxsys[i+1]):
            j = lor[jdx]
            ais[z[j]]+=system[jdx]
        slor=np.nonzero(ais)[0]
        sumlor+=len(slor)
        idxsys_z[i+1] = sumlor
        hlor_z[idxsys_z[i]:idxsys_z[i+1],0] = slor
        hlor_z[idxsys_z[i]:idxsys_z[i+1],1] = ais[slor]
    return hlor_z[:sumlor]

# @jit(cache=True)
def segment_system_matrix(idxsys, lor, system, z):
    M = len(idxsys)-1
    idxsys_z = np.zeros(M+1, dtype = np.int32)
    hlor_z = inner_segment_system_matrix(idxsys, lor, system, z, idxsys_z)
    jsys_z = hlor_z[:, 0].astype(np.int32)
    asys_z = hlor_z[:, 1].astype(np.float64)
    return idxsys_z, jsys_z, asys_z

@njit(float64[:, ::1](int32[::1], int32[::1], float64[::1], int32[::1], int32[::1], float64[::1], int32[::1]), cache=True)
def inner_frame_system_matrix(idxsys, lor, system, idxframe, frame_index, frame_weight, idxsys_z):
    M = len(idxsys)-1
    S = len(idxframe)-1
    hlor_z = np.zeros((M*S,2), dtype = np.float64)
    sumlor = 0
    ais = np.zeros(S, dtype = np.float64)
    for i in range(M):
        # print('lor',i,M,sumlor)
        arg = np.argsort(lor[idxsys[i]:idxsys[i+1]])+idxsys[i]
        jsys = lor[arg]
        asys = system[arg]
        ais[:] = 0.
        # print(jsys)
        for s in range(S):
            jfra = frame_index[idxframe[s]:idxframe[s+1]]
            afra = frame_weight[idxframe[s]:idxframe[s+1]]
            js=0
            jf=0
            while js<len(jsys) and jf<len(jfra):
                if jsys[js]==jfra[jf]:
                    ais[s]+=asys[js]*afra[jf]
                    js+=1
                    jf+=1
                elif jsys[js] < jfra[jf]:
                    js+=1
                else:
                    jf+=1
        slor=np.nonzero(ais)[0]
        sumlor+=len(slor)
        idxsys_z[i+1] = sumlor
        hlor_z[idxsys_z[i]:idxsys_z[i+1],0] = slor
        hlor_z[idxsys_z[i]:idxsys_z[i+1],1] = ais[slor]
    return hlor_z[:sumlor]

def frame_system_matrix(idxsys, lor, system, frame):
    M = len(idxsys)-1
    idxsys_z = np.zeros(M+1, dtype = np.int32)
    hfr = np.hstack(frame)
    idxframe = np.r_[0,np.cumsum([v.shape[1] for v in frame])].astype(np.int32)
    # print(hfr.shape,idxframe[-1],len(idxframe))
    hlor_z = inner_frame_system_matrix(idxsys, lor, system, idxframe, \
        hfr[0].astype(np.int32), hfr[1].astype(np.float64), idxsys_z)
    jsys_z = hlor_z[:, 0].astype(np.int32)
    asys_z = hlor_z[:, 1].astype(np.float64)
    return idxsys_z, jsys_z, asys_z, idxframe, hfr[0].astype(np.int32), hfr[1].astype(np.float64) 

@njit(float64[:, ::1](int32[::1], int32[::1], float64[::1], int32[:,::1], int32[::1], int32[::1]), cache=True)
def inner_concat_segment_system_matrix(idxsys, lor, system, zm, S_cc, idxsys_cc):
    K = len(zm)
    M = len(idxsys)-1
    idx_cz = np.zeros((K,M+1), dtype = np.int32)
    idx_zc = np.zeros(K+1, dtype = np.int32)
    hlor_cz = np.zeros((0,2), dtype = np.float64)
    for k in range(K):
        hlor_z = inner_segment_system_matrix(idxsys, lor, system, zm[k], idx_cz[k])
        hlor_z[:,0]+= S_cc[k]
        hlor_cz = np.concatenate((hlor_cz, hlor_z))
        idx_zc[k+1] = idx_zc[k]+idx_cz[k,-1]
        S_cc[k+1] = S_cc[k]+len(np.unique(zm[k]))
    hlor_cc = np.empty((idx_zc[-1],2), dtype = np.float64)
    idx = 0
    for i in range(M):
        for k in range(K):
            hlor_cc[idx:idx+idx_cz[k,i+1]-idx_cz[k,i]] = hlor_cz[idx_zc[k]+idx_cz[k,i]:idx_zc[k]+idx_cz[k,i+1]]
            idx+=idx_cz[k,i+1]-idx_cz[k,i]
        idxsys_cc[i+1] = idx
    return hlor_cc

def concat_segment_system_matrix(idxsys, lor, system, z):
    M = len(idxsys)-1
    K = len(z)
    S_cc = np.zeros(K+1, dtype = np.int32)
    idxsys_cc = np.zeros(M+1, dtype = np.int32)
    hlor_cc = inner_concat_segment_system_matrix(idxsys, lor, system, z, S_cc, idxsys_cc)
    jsys_cc = hlor_cc[:, 0].astype(np.int32)
    asys_cc = hlor_cc[:, 1].astype(np.float64)
    return idxsys_cc, jsys_cc, asys_cc, S_cc

@njit(float64[::1](int32[::1], int32[::1], float64[::1], float64[::1]), cache=True)
def segment_project(idxsys_z, lor_z, system_z, mu_z):
    M = len(idxsys_z)-1
    sino_z = np.zeros(M, dtype = np.float64)
    for i in range(M):
        for jdx in range(idxsys_z[i], idxsys_z[i+1]):
            j = lor_z[jdx]
            sino_z[i]+=system_z[jdx]*mu_z[j]    
    return sino_z

@njit(float64[:,::1](int32[::1], int32[::1], float64[::1], int32[::1], int32[::1],), cache=True)
def inner_mask_system_matrix(idxsys, lor, system, mask, idxsys_z):
    hlor_z = np.empty((len(lor),2), dtype = np.float64)
    M = len(idxsys)-1
    idxsys_z[0]=0
    sum_jz=0
    for i in range(M):
        for jdx in range(idxsys[i], idxsys[i+1]):
            j = lor[jdx]
            if mask[j] > 0:
                hlor_z[sum_jz,0] = j
                hlor_z[sum_jz,1] = system[jdx]
                sum_jz+=1
        idxsys_z[i+1]=sum_jz      
    return hlor_z[:sum_jz]

def mask_system_matrix(idxsys, lor, system, mask):
    M = len(idxsys)-1
    idxsys_z = np.empty(M+1, dtype=np.int32)
    hlor_z = inner_mask_system_matrix(idxsys, lor, system, mask.astype(np.int32), idxsys_z)
    return idxsys_z, np.array(hlor_z[:,0], order = 'C', dtype=np.int32), np.array(hlor_z[:,1], order = 'C')

@njit(float64[::1](int32[::1], int32[::1], float64[::1]), cache=True)
def system_condition_number(idxsys, lor, system):
    M = len(idxsys)-1
    K = len(np.unique(lor))
    A = np.zeros((K,K), dtype = np.float64)
    for i in range(M):
        for jdx1 in range(idxsys[i],idxsys[i+1]):
            for jdx2 in range(idxsys[i],idxsys[i+1]):
                A[lor[jdx1],lor[jdx2]]+=system[jdx1]*system[jdx2]
    eigens = np.linalg.eigvalsh(A)
    cn = np.sqrt(max(eigens)/min(eigens))
    # print('condition number', np.sqrt(max(eigens)/min(eigens)))
    return np.sqrt(eigens)
###############################################################################

@njit(int32[::1](float64[::1], int32[::1], int32[::1], float64[::1], float64), cache = True)
def generate_data_sys(phantom, idxsys, lor, system, T):
    M = len(idxsys)-1
    N = np.random.poisson(T)
    # n = phantom.size
    nu = np.zeros(M, dtype = np.float64)
    hsino = np.zeros(M, dtype = np.int32)
    for i in range(M):
        for jdx in range(idxsys[i], idxsys[i+1]):
            j = lor[jdx]
            aij = system[jdx]
            nu[i]+=phantom[j]*aij
    q = nu/np.sum(nu)
    idxq = np.nonzero(q)[0]
    print("M",M,len(idxq))
    sino = np.random.multinomial(N, q[idxq])
    hsino[idxq] = sino[:]
    return hsino

###############################################################################
###############################################################################    
@njit(float64[::1](int32[::1], int32[::1], float64[::1], int32[::1], float64[::1], float64[::1], int64, boolean), cache=True)
def ml_em_tomo(idxsys, lor, system, sino_ind, sino_counts, lamb_ini, L, verbose):
    # ret = np.empty(L, dtype = np.float64)
    n = max(lor)+1 # np.prod(dims)
    M = len(idxsys) - 1
    y = sino_counts
    indy = sino_ind
    m = len(y)
    sy = np.sum(y)
    # print(M, m, n, np.min(system))
    A = np.zeros(n, dtype = np.float64)
    for i in range(M):
        for jdx in range(idxsys[i], idxsys[i+1]):
            j = lor[jdx]
            A[j] += system[jdx]
    if len(lamb_ini) == 0:
        lambda_ = np.ones(n, dtype = np.float64)*sy/float(n)/A
    else:
        lambda_ = lamb_ini[:]
    # print(np.argmin(A), min(A), A[:10])
    for l in range(L):
        ni = np.zeros(n, dtype = np.float64)
        for idx in range(m):
            i = indy[idx]
            di = 0.
            for jdx in range(idxsys[i], idxsys[i+1]):
                j = lor[jdx]
                aij = system[jdx]
                di += aij*lambda_[j]
            if di > 0. and y[idx] > 0:
                yi = y[idx]/di
                for jdx in range(idxsys[i], idxsys[i+1]):
                    j = lor[jdx]
                    aij = system[jdx]
                    ni[j] += aij*yi
        # print('**')
        for j in range(n):
            lambda_[j] *= ni[j]/A[j]
        if verbose:
            llik = 0.
            for idx in range(m):
                i = indy[idx]
                lambi = 0.
                for jdx in range(idxsys[i], idxsys[i+1]):
                    j = lor[jdx]
                    aij = system[jdx]
                    lambi+=aij*lambda_[j]
                llik+=y[idx]*log(lambi)-lambi
            print('iter',l, 'llik', llik)
        # ret[l] = llik
    return lambda_

###############################################################################

@njit(float64[::1](int32[::1], int32[::1], float64[::1], int32[::1], int32[::1], float64[::1], int64, int64, boolean), cache=True)
def ml_cosem_tomo(idxsys, lor, system, sino_ind, sino_counts, lambda_ini, B, L, verbose):
    # ret = np.empty(L, dtype = np.float64)
    n = max(lor)+1 # np.prod(dims)
    M = len(idxsys) - 1
    y = sino_counts
    indy = sino_ind
    m = len(y)
    sy = np.sum(y)
   
    # print(M, m, n, np.min(system))
    A = np.zeros(n, dtype = np.float64)
    ni = np.ones((n,B), dtype = np.float64)*sy/float(n*B)
    if len(lambda_ini) == 0:
        lambda_ = np.ones(n, dtype = np.float64)*sy/float(n)#/A
    else:
        lambda_ = lambda_ini[:]
    idxos = np.zeros(B+1, dtype = np.int32)
    subsets = np.empty(m, dtype = np.int32)
    for idx in range(m):
        i = indy[idx]
        b = i % B
        idxos[b+1]+=1
        subsets[idx] = b
    subsets = np.argsort(subsets)
    idxos = np.cumsum(idxos)

    # print(np.argmin(A), min(A), A[:10])
    for l in range(L):
        for b in range(B):
            if l == 0:
                for i in range(b, M, B):
                    for jdx in range(idxsys[i], idxsys[i+1]):
                        j = lor[jdx]
                        A[j] += system[jdx]                           
            ni[:,b] = 0.
            #np.zeros(n, dtype = np.float64)
            #for idx in range(m):
            for indb in range(idxos[b], idxos[b+1]):
                idx = subsets[indb]
            # for idx in range(b, m, B):
                i = indy[idx]
                di = 0.
                for jdx in range(idxsys[i], idxsys[i+1]):
                    j = lor[jdx]
                    aij = system[jdx]
                    di += aij*lambda_[j]
                if di > 0. and y[idx] > 0:
                    yi = y[idx]/di
                    for jdx in range(idxsys[i], idxsys[i+1]):
                        j = lor[jdx]
                        aij = system[jdx]
                        ni[j, b] += aij*yi#*lambda_[j]
            # print('**')

            for j in range(n):
                ni[j, b]*=lambda_[j]
                Nj = np.sum(ni[j])
                if Nj != 0.:
                    lambda_[j] = Nj/A[j]
        if verbose:
            llik = 0.
            for idx in range(m):
                i = indy[idx]
                lambi = 0.
                for jdx in range(idxsys[i], idxsys[i+1]):
                    j = lor[jdx]
                    aij = system[jdx]
                    lambi+=aij*lambda_[j]
                llik+=y[idx]*log(lambi)-lambi
            # ret[l] = llik
            print('iter',l, 'llik', llik)
    return lambda_

###############################################################################

@njit(float64[::1](int32[::1], int32[::1], float64[::1], int32[::1], float64[::1], int32[::1], int32[:,::1], float64[::1], float64, float64, float64, int64, int64, boolean), cache=True)
def map_cosem_tomo(idxsys, lor, system, sino_ind, sino_counts, idxnb, neighbors, lambda_ini, beta, eta, nu, S, L, verbose):
    # ret = np.empty(L, dtype = np.float64)
    # S = 1
    n = max(lor)+1 # np.prod(dims)
    M = len(idxsys) - 1
    indy = sino_ind
    m = len(sino_counts)
    sy = np.sum(sino_counts)
    a0 = 1.
    # b0 = sy/float(n)*3
    y = sino_counts#.astype(np.float64)
    weight = np.empty(len(neighbors), dtype = np.float64)
    for j in range(n):
        for indj in range(idxnb[j]+1, idxnb[j+1]):
            weight[indj] = 1./sqrt(neighbors[indj,1])   
    A = np.zeros(n, dtype = np.float64)
    for i in range(M):
        for jdx in range(idxsys[i], idxsys[i+1]):
            j = lor[jdx]
            A[j] += system[jdx]
    ni = np.ones((n,S), dtype = np.float64)*sy/float(n*S)
    # ni = np.zeros((n,S), dtype = np.float64)
    if len(lambda_ini) == 0:
        lambda_ = np.ones(n, dtype = np.float64)*sy/float(n)#/A
    else:
        lambda_ = lambda_ini[:]
    lambda_new = np.zeros(n, dtype = np.float64)
    # lambda_old = np.ones(n, dtype = np.float64)*sy/float(n)#/A
    idxos = np.zeros(S+1, dtype = np.int32)
    subsets = np.empty(m, dtype = np.int32)
    for idx in range(m):
        i = indy[idx]
        ib = np.int32(i % S)
        subsets[idx] = ib
        idxos[ib+1]+=1
    subsets = np.argsort(subsets).astype(np.int32)
    idxos = np.cumsum(idxos).astype(np.int32)
    # print(np.argmin(A), min(A), A[:10])
    for l in range(L):
        for ib in range(S):
            ni[:,ib] = 0.
            #np.zeros(n, dtype = np.float64)
            #for idx in range(m):
            for indb in range(idxos[ib], idxos[ib+1]):
                idx = subsets[indb]
            # for idx in range(b, m, B):
                i = indy[idx]
                di = 0.
                for jdx in range(idxsys[i], idxsys[i+1]):
                    j = lor[jdx]
                    aij = system[jdx]
                    di += aij*lambda_[j]
                if di > 0. and y[idx] > 0:
                    yi = y[idx]/di
                    for jdx in range(idxsys[i], idxsys[i+1]):
                        j = lor[jdx]
                        aij = system[jdx]
                        ni[j, ib] += aij*yi#*lambda_[j]
            for j in range(n):
                ni[j, ib]*=lambda_[j]
                Nj = np.sum(ni[j]) + a0 - 1.
                U = V = 0. #
                for indj in range(idxnb[j]+1, idxnb[j+1]):
                    jj = neighbors[indj,0]
                    tj = (lambda_[j]-lambda_[jj]) #(lambda_old[j]-lambda_old[jj]) # 
                    sj = lambda_[j]+lambda_[jj]#.5*(lambda_old[jj]+lambda_old[j])
                    wj = weight[indj]     
                    r = wj*nu#
                    U += r
                    V += r*sj
                    if tj!=0.:
                        r = wj*(1-nu)*tanh(eta*tj)/tj# logcosh
                    else:
                        r = wj*(1-nu)*eta#
                    # r = wj*(1-nu)*eta/(1.+eta*fabs(tj)) # Lange (1-nu)
                    U += r #t
                    V += r*sj
                x_reg = .5*V/U
                x_em = Nj/A[j]
                a = 2.*beta*U/A[j]
                b = 1-a*x_reg
                lambda_new[j] = 2*x_em/(sqrt(b**2+4*a*x_em)+b)
                if lambda_new[j] < 1e-30:
                    lambda_new[j] = 1e-30
            lambda_[:] = lambda_new[:]
        # a0 = gamma_shape_fit(lambda_[lambda_>1e-6], 10.)
        if verbose:
            llik = 0.
            for idx in range(m):
                i = indy[idx]
                lambi = 0.
                for jdx in range(idxsys[i], idxsys[i+1]):
                    j = lor[jdx]
                    aij = system[jdx]
                    lambi+=aij*lambda_[j]
                llik+=y[idx]*log(lambi)-lambi
            print('iter',l, 'llik', llik, a0)
    ## lambda_bb[iwb, :] =  lambda_
    return lambda_
        
###############################################################################
###############################################################################
def ml_em_anat_seg(idxsys, lor, system, sino_ind, sino_counts, idxnb, neighbors, beta, eta, nu, S, L, tag, verbose):
    # tag = np.load(np.random.choice(list_segs,1)[0])
    M = len(idxsys) - 1
    # sino_ind = np.nonzero(hsino)[0].astype(np.int32)
    # sino_counts = hsino[sino_ind].astype(np.float64)
    idxsys_z = np.zeros(M+1, dtype = np.int32)
    hlor_z = inner_segment_system_matrix(idxsys, lor, system, tag, idxsys_z)
    lor_z = hlor_z[:, 0].astype(np.int32)
    system_z = hlor_z[:, 1].astype(np.float64)
    # idxsys_z, lor_z, system_z = segment_system_matrix(idxsys, lor, system, tag)#anat_z.ravel())
    mu_z = ml_em_tomo(idxsys_z, lor_z, system_z, sino_ind, sino_counts, np.array([]), L, verbose)#[anat_z].ravel()
    hsino_z = segment_project(idxsys_z, lor_z, system_z, mu_z)
    return mu_z, hsino_z

###############################################################################

@njit(float64[::1](int32[::1], int32[::1], float64[::1], int32[::1], int32[::1], int32[:,::1], float64, float64, float64, int64, int64, float64, float64[::1], float64, boolean), cache=True)
def map_cosem_anat_tomo(idxsys, lor, system, hsino, idxnb, neighbors, beta, eta, nu, S, L, t, hsino_z, rho, verbose):
    #
    hsino_mod = (hsino.astype(np.float64)+rho*hsino_z)/(1.+rho)
    sino_ind_mod = np.nonzero(hsino_mod)[0].astype(np.int32)
    sino_counts_mod = hsino_mod[sino_ind_mod]
    lambda_ = map_cosem_tomo(idxsys, lor, system, sino_ind_mod, sino_counts_mod/t, idxnb, neighbors, 
    np.empty(0,dtype=np.float64), beta, eta, nu, S, L, verbose)
    return lambda_

###############################################################################
#      
@njit(void(int64, int32[::1], int32[::1], float64[::1], int32[::1], int32[::1], int32[:,::1], float64, float64,
float64, int64, int64, int64, float64, float64, int32[::1], int32[::1], float64[::1], boolean, int32[::1],
float64[::1], float64, float64[::1],float64[::1],float64[::1]), cache=True)#
def inner_npl_map_cosem_anat_tomo(iwb, idxsys, lor, system, hsino, idxnb, neighbors, beta, eta, nu, S, L, B, t,
 rho,  idxsys_z, lor_z, system_z, verbose, sino_ind, y, sy, mu_ini, lambda_ini, lambda_b):
    print('iter npl',iwb)
    # M = len(idxsys) - 1
    m = len(y)
    if rho>0.:
        wb_z = np.empty(m, dtype = np.float64) 
        for i in range(m):
            wb_z[i]= np.random.gamma(y[i])
        # y_z = wb_z/np.sum(wb_z)*sy       
        mu_z = ml_em_tomo(idxsys_z, lor_z, system_z, sino_ind, wb_z, mu_ini, L, False)#mu_ini[p] y_z
        gamma_b = segment_project(idxsys_z, lor_z, system_z, mu_z)
        # bias data with anat prior
        hsino_b = (hsino.astype(np.float64)+rho*gamma_b)
        sino_ind_b = np.nonzero(hsino_b)[0].astype(np.int32)
        # draw lambda with wbb
        m_b = len(sino_ind_b)
        wb_b = np.empty(m_b, dtype = np.float64)
        for i in range(m_b):
            wb_b[i]= np.random.gamma(hsino_b[sino_ind_b[i]])/(1.+rho)
    else:
        wb_b = np.empty(m, dtype = np.float64)
        sino_ind_b = sino_ind[:]
        for i in range(m):
            wb_b[i]= np.random.gamma(y[i])
    # y_b = wb_b/np.sum(wb_b)*sy
    lambda_b[:] = map_cosem_tomo(idxsys, lor, system, sino_ind_b, wb_b/t, idxnb, neighbors,lambda_ini, 
    beta, eta, nu, S, L, verbose)[:] #lambda_ini y_b
    return
#    
@njit(float64[:,::1](int32[::1], int32[::1], float64[::1], int32[::1], int32[::1], int32[:,::1],
float64, float64, float64, int64, int64, int64, float64, float64, int32[::1], int32[::1], float64[::1], 
float64[::1], float64[::1], boolean), parallel=True, cache = True)
def npl_map_cosem_anat_tomo(idxsys, lor, system, hsino, idxnb, neighbors, beta, eta, nu, S, L, B, t, rho, 
idxsys_z, lor_z, system_z, mu_ini, lambda_ini, verbose):
    n = max(lor)+1 # np.prod(dims)
    sino_ind =np.nonzero(hsino)[0].astype(np.int32)
    y = hsino[sino_ind].astype(np.float64)
    sy = np.sum(y)   
    lambda_bb = np.empty((B,n), dtype = np.float64)
    # 
    for iwb in prange(B):
        inner_npl_map_cosem_anat_tomo(iwb, idxsys, lor, system, hsino, idxnb, neighbors, beta, eta, nu, S, L, B, t, rho,
         idxsys_z, lor_z, system_z, verbose, sino_ind, y, sy, mu_ini, lambda_ini, lambda_bb[iwb])
    return lambda_bb
###############################################################################
#      
###############################################################################
#   
@njit(void(int32[::1], int32[::1], float64[::1], int32[::1], int32[::1], int32[::1], int32[:,::1], float64, float64, float64, int64, int64, int64, boolean, int64, float64, float64[::1], float64[::1]), cache=True)#, cache = True
def inner_wbb_map_cosem_tomo(idxsys, lor, system, sino_ind, sino_counts, idxnb, neighbors, beta, eta, nu, S, L, B, verbose, 
        iwb, sy, lambda_ini, lambda_b):
    print('iter wbb',iwb)
    m = len(sino_counts)
    wbb = np.empty(m, dtype = np.float64)
    w0 = 1.
    # while w0<0.05:
    #     w0 = np.random.gamma(1.)
    for i in range(m):
        wbb[i]= np.random.gamma(sino_counts[i])
    betw = beta*w0#/norm
    lambda_b[:] = map_cosem_tomo(idxsys, lor, system, sino_ind, wbb, idxnb, neighbors, lambda_ini, betw, eta, nu, S, L, verbose)[:] #y
    return
#      
@njit(float64[:,::1](int32[::1], int32[::1], float64[::1], int32[::1], int32[::1], int32[::1], int32[:,::1], float64, float64, float64, int64, int64, int64, boolean), parallel=True, cache = True)
def wbb_map_cosem_tomo(idxsys, lor, system, sino_ind, sino_counts, idxnb, neighbors, beta, eta, nu, S, L, B, verbose):
    n = max(lor)+1 # np.prod(dims)
    # m = len(sino_counts)
    sy = np.sum(sino_counts)
    lambda_bb = np.empty((B,n), dtype = np.float64)
    lambda_ini = map_cosem_tomo(idxsys, lor, system, sino_ind, sino_counts.astype(np.float64), idxnb, neighbors, 
    np.empty(0,dtype=np.float64), beta, eta, nu, S, L, verbose)
    for iwb in prange(B):
        inner_wbb_map_cosem_tomo(idxsys, lor, system, sino_ind, sino_counts, idxnb, neighbors, beta, eta, nu, S, L, B, verbose,
        iwb, sy, lambda_ini, lambda_bb[iwb])
    return lambda_bb

###############################################################################
