#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
# import math
from math import sqrt, log, exp, fabs, floor, ceil, cos, sin, atan2, lgamma, gamma, tanh, factorial, pi, nan, log1p
from random import random, shuffle
from numba import njit, float64, int64, int32, void, boolean, prange, vectorize

###############################################################################


@njit(int32(int32[::1]), nogil=True, cache = True)
def reorder_segments(tag):
    n = tag.size
    flag = -np.ones(n+1, dtype=np.int32)  # label->n° segment
    nsegs = 0
    for j in range(n):
        tagj = tag[j]
        if tagj >= 0:
            if flag[tagj] < 0:
                flag[tagj] = nsegs
                nsegs += 1
    for j in range(n):  # REORDONNANCEMENT
        tagj = tag[j]
        if tagj >= 0:
            tag[j] = flag[tagj]
    return nsegs

########################################################################

@njit(void(int32, int32, int32, int32, int32[::1], int32[::1], int32[:, ::1]), nogil=True)#, cache = True
def rind(p, nor, nbdim, diagonal, elm, pos,  dec):
    for i in range(3):  # np.array([0,1,-1],dtype=np.int32):
        elm[p] = ((i+1) % 3)-1
        normelm = nor+abs(elm[p])
        if p < nbdim-1:
            rind(p+1, normelm, nbdim, diagonal, elm, pos, dec)
        elif normelm <= diagonal:
            dec[pos[0],:-1] = np.copy(elm)
            dec[pos[0],-1] = normelm
            pos[0] += 1


@njit(int32[:, ::1](int32, int32), nogil=True, cache = True)
def connex(nbdim, diagonal):
    nmax = 3**nbdim
    dec = np.zeros((nmax, np.int64(nbdim+1)), dtype=np.int32)
    elm = np.zeros(nbdim, dtype=np.int32)
    pos = np.zeros(1, dtype=np.int32)
    rind(0, 0, nbdim, diagonal, elm, pos, dec)
    return dec[:pos[0]]


@njit(int32[::1](int32, int32[::1]), nogil=True, cache = True)
def unravel_ind(ind, dims):
    nbdim = len(dims)
    idx = np.empty(nbdim, dtype=np.int32)
    v = ind
    for idim in range(nbdim-1, 0, -1):
        ndim = dims[idim]
        idx[idim] = v % ndim
        v //= ndim
    idx[0] = v
    return idx


@njit(int32(int32[::1], int32[::1]), nogil=True, cache = True)
def ravel_ind(idx, dims):
    nbdim = len(dims)
    delta = 1
    ind = 0
    for idim in range(nbdim-1, -1, -1):
        d = idx[idim]
        ind += delta*d
        delta *= dims[idim]
    return ind


@njit(int32[:,::1](int32[::1], int32[:, ::1], int32[::1], int32[::1]), nogil=True, cache = True)
def get_voisins(dims, connx, tag, index_voisins):
    lg = np.prod(dims)
    nbdim = dims.shape[0]
    coov = np.empty(nbdim, dtype=np.int32)
    voisins = -np.ones((lg*connx.shape[0],2), dtype=np.int32)
    index_voisins[0] = 0
    idv = 0
    for ind in range(lg):
        if tag[ind] != -1:
            coord = unravel_ind(ind, dims)
            kf = 0
            lc = len(connx)
            for k in range(lc):
                flag = True
                for i in range(nbdim):
                    coov[i] = coord[i]+connx[k, i]
                    if coov[i] < 0 or coov[i] > dims[i]-1:
                        flag = False
                        break
                if flag:  # && g_tag[indv]>=0
                    indv = ravel_ind(coov, dims)
    #                 voisins[ind,k]=indv
                    if tag[indv] != -1:
                        voisins[idv+kf,0] = indv
                        voisins[idv+kf,1] = connx[k, -1]
                        kf += 1
            idv += kf
        index_voisins[ind+1] = idv

    return voisins[:idv]

@njit(int32[::1](int32, int32[::1], int32[:, ::1], int32[::1]), nogil=True, cache = True) #c
def my_voisins(ind, dims, connx, tag):
    nbdim = dims.shape[0]
    coov = np.empty(nbdim, dtype=np.int32)
    voisins = -np.ones(connx.shape[0], dtype=np.int32)
    coord = unravel_ind(ind, dims)
    kf = 0
    lc = len(connx)
    for k in range(lc):
        flag = True
        for i in range(nbdim):
            coov[i] = coord[i] + connx[k, i]
            if coov[i] < 0 or coov[i] > dims[i] - 1:
                flag = False
                break
        if flag:  # && g_tag[indv]>=0
            indv = ravel_ind(coov, dims)
#                 voisins[k]=indv
            if tag[indv] != -1:
                voisins[kf] = indv
                kf += 1
    return voisins[:kf]


def get_neighbors(dims, diagonal, tag):
    nsegs = len(tag)
    # print(nsegs)
    nbdim = dims.size
    connx = connex(nbdim, diagonal)
    index_neighbors = np.zeros(nsegs+1, dtype=np.int32)
    neighbors = get_voisins(dims, connx, tag, index_neighbors)
    return neighbors, index_neighbors
#######################################################################


def my_boundaries(bds, xs, ys):
    lines = my_boundaries_ext(bds, xs, ys)
    lines[2::3] = [None, None]
    return lines


@njit(float64[:, :](int32[:, :], float64[:], float64[:]), nogil=True, cache=True)
def my_boundaries_ext(bds, xs, ys):
    #     print(bds.shape)
    # ~ l=np.zeros((bds.size*3,2),dtype=np.float64)
    l = np.zeros((len(xs) * len(ys) * 6, 2), dtype=np.float64)
#     yl=np.empty(bds.size*2,dtype=np.float64)
    k = 0
    vbounds = (bds[:, 1:] - bds[:, :-1]) != 0
    for j in range(len(ys) - 1):
        v = xs[1:-1][vbounds[j]]
        for i in range(len(v)):
            l[k, 0] = v[i]
            l[k, 1] = ys[j]
            l[k + 1, 0] = v[i]
            l[k + 1, 1] = ys[j + 1]
#             l[k+2,0]=NaN
#             l[k+2,1]=NaN
            k += 3
    hbounds = (bds[1:, :] - bds[:-1, :]) != 0
    for i in range(len(xs) - 1):
        h = ys[1:-1][hbounds[:, i]]
        for j in range(len(h)):
            l[k, 0] = xs[i]
            l[k, 1] = h[j]
            l[k + 1, 0] = xs[i + 1]
            l[k + 1, 1] = h[j]
#             l[k+2,0]=NaN
#             l[k+2,1]=NaN
            k += 3
    return l[:k]
########################################################################
@njit(int32(int32, int32[::1], int32[::1], int32[::1]), nogil=True, cache = True) #c
def split_segment3(origine0, precedents_root, precedents_chain, listind):
    listind[0] = origine0
    ifirst = 0
    ilast = 1
    fin = False
    while not fin:
        icur = ilast
        for ind in range(ifirst, ilast):
            p = precedents_root[listind[ind]]
            while(p >= 0):  # and p!=origine0:
                listind[icur] = p
                icur += 1
                p = precedents_chain[p]
        if icur == ilast:
            fin = True
        else:
            ifirst = ilast
            ilast = icur
    return ilast


@njit(int32(int32, int32[::1], int32[::1], int32[::1], int32[::1], float64[::1], float64, int32), nogil=True, cache = True) #c
def tag_positive_index(origine0, index_neighbors, neighbors, listind, tag, weights, threshold, nseg):
    listind[0] = origine0
    tag[origine0] = nseg
    ifirst = 0
    ilast = 1
    fin = False
    while not fin:
        icur = ilast
        for i in range(ifirst, ilast):
            indi = listind[i]
            # ~ tag[indi]=nseg
            for j in range(index_neighbors[indi] + 1, index_neighbors[indi + 1]):
                indj = neighbors[j]
                if tag[indj] == -1 and weights[j] > threshold:
                    tag[indj] = nseg
                    listind[icur] = indj
                    icur += 1
        if icur == ilast:
            fin = True
        else:
            ifirst = ilast
            ilast = icur
    return ilast


@njit(int32(int32, int32[::1], int32[::1], int32[::1], float64[::1], float64), nogil=True, cache=True)
def tag_positive_connect(n, index_neighbors, neighbors, tag, weights, threshold):
    nseg = 0
    sumsize = 0
    # ~ print('n',n)
    listind = np.empty(n, dtype=np.int32)
    for ind in range(n):
        if tag[ind] == -1:
            # ~ print('ind',ind,nseg)
            sizeseg = tag_positive_index(
                ind, index_neighbors, neighbors, listind, tag, weights, threshold, nseg)
            sumsize += sizeseg
            # ~ print('size',sizeseg,sumsize)
            nseg += 1
    return nseg


@njit(void(int32, int32[::1], int32[::1], int32[::1], int32[::1]), nogil=True, cache=True)
def index_weights(n, index_neighbors, neighbors, tag, weightscum):
    for ind in range(n):
        for inext in range(index_neighbors[ind], index_neighbors[ind + 1]):
            indnext = neighbors[inext]
            if tag[ind] == tag[indnext]:
                weightscum[inext] += 1
    return

@njit(void(int32, int32, int32[::1], int32[::1]), nogil=True, cache = True) #c
def remove_link(ind, suc, precedents_root, precedents_chain):
    pr = precedents_root[suc]
    last_pr = -1
    while pr != ind:
        last_pr = pr
        pr = precedents_chain[pr]
    next_pr = precedents_chain[pr]
    if last_pr >= 0:
        precedents_chain[last_pr] = next_pr
    else:
        precedents_root[suc] = next_pr
    precedents_chain[pr] = -1
    return


@njit(void(int32, int32, int32[::1], int32[::1]), nogil=True, cache = True) #
def add_link(ind, suc, precedents_root, precedents_chain):
    pr = precedents_root[suc]
    precedents_root[suc] = ind
    precedents_chain[ind] = pr
    return
########################################################################


@njit(float64(float64, float64, float64[::1]), nogil=True, cache = True)#c
def logZNIGv(kappa, half_nu, nu_sig2):
    nbmod = len(nu_sig2)
    p = lgamma(half_nu) - .5 * log(kappa)
    res = 0.
    for m in range(nbmod):
        res += log(nu_sig2[m])
    return nbmod * p - half_nu * res


@njit(float64(float64, float64, float64[::1]), nogil=True, cache = True) #c
def logZNIGm(p, half_nu, nu_sig2):
    nbmod = len(nu_sig2)
    res = 0.
    for m in range(nbmod):
        res += log(nu_sig2[m])
    return nbmod * p - half_nu * res
########################################################################


@njit((float64[:, :], int32, int32[::1], float64[:, :], int32[::1], float64[::1], boolean[::1]), nogil=True, cache=True) #c
def updatevar(im0, nbmod, sum0, sumx, tag, sig2, updatevariance):
    for m in range(nbmod):
        if updatevariance[m]:
            moy = sumx[tag[tag != -1], m] / \
                sum0[tag[tag != -1]].astype(np.float64)
            sig2[m] = np.var(im0[tag != -1, m] - moy)
########################################################################


@njit(float64[::1](float64[:, :], int32[::1]), nogil=True, cache=True)
def computevar(im0, tag):
    n = im0.shape[0]
    nbmod = im0.shape[1]
    sig2 = np.empty((nbmod), dtype=np.float64)
    sum0 = np.zeros(n, dtype=np.int32)
    sumx = np.zeros((n, nbmod), dtype=np.float64)
    for i in range(n):
        tagi = tag[i]
        if tagi >= 0:
            for m in range(nbmod):
                sumx[tagi, m] += im0[i, m]
            sum0[tagi] += 1
    for m in range(nbmod):
        moy = sumx[tag[tag != -1], m] / sum0[tag[tag != -1]].astype(np.float64)
        sig2[m] = np.var(im0[tag != -1, m] - moy)
    return sig2
########################################################################


@njit(int32(int32, float64[::1]), nogil=True, cache=True)#
def randint_choice(r, proba):
    cum = np.sum(proba)
    rnd = random() * cum
    cum = 0.
#     r=proba.shape[0]
    for ir in range(r):
        cum += proba[ir]
        if cum >= rnd:
            return ir
    ir = r - 1
    print("overflow rand")
    return ir  # min(ir,r-1)

###############################################################################
########################################################################
@njit(int32[:,::1](float64[:, :], int32[::1], int32, int32, float64, float64, float64, float64[::1], int32[::1], int32[::1], int32[::1], int32, int32, int32), nogil=True, cache = True)  # int32[::1]
def segmentation(im0, dims, nbiter, diagonal, alpha, kappa0, nu0, sig2, tag, neighbors, index_neighbors, SIZE_MAX_SEGMENT, burn, decim):
    #     init
    n = im0.shape[0]
    nbmod = im0.shape[1]
    nmask = np.sum(tag != -1)
    tagr = np.empty(n, dtype=np.int32)
    ret = np.empty(((nbiter-burn)//decim, n), dtype=np.int32)
    if neighbors is None or index_neighbors is None:
        nbdim = dims.shape[0]
        cnx = connex(nbdim, diagonal)
        neighbors_in_time = True
    else:
        neighbors_in_time = False
    print('parameters: alpha = ', alpha, 'nu0 = ', nu0, 'kappa0 = ', kappa0)
    print("nb vertices :", n, 'nb. mod :', nbmod,
          'shape :', dims, 'nb mask :', nmask)
    successeur = np.empty(n, dtype=np.int32)
    # ~ tag=np.empty(n,dtype=np.int32)
    # ~ tag=mask
    # ~ maxcnx=cnx.shape[0]
    # ~ voisins=get_voisins(dims,cnx,tag)
    # ~ precedents=-np.ones((n,maxcnx),dtype=np.int32)
    precedents_root = -np.ones(n, dtype=np.int32)
    precedents_chain = -np.ones(n, dtype=np.int32)
    # ~ tag=np.empty(n,dtype=np.int32)
    # ~ tag[:]=tag0[:]

    # ~ tag_max=np.empty(n,dtype=np.int32)
    # ~ tag_max[:]=tag0[:]
    listind0 = np.empty(SIZE_MAX_SEGMENT, dtype=np.int32)
    sum0 = np.empty(n, dtype=np.int32)
    sumx = np.empty((n, nbmod), dtype=np.float64)
    sumx2 = np.empty((n, nbmod), dtype=np.float64)
    countx = np.empty((nbmod), dtype=np.float64)
    countx2 = np.empty((nbmod), dtype=np.float64)
    labels_libres = -np.ones(n, dtype=np.int32)
    # ~ labels_libres[0]=n
    ind_label = 0
    nbtag = 0  # n
    llik = 0.
    updatevar = np.zeros((nbmod), dtype=np.bool_)
    # ~ sig2=np.empty(nbmod,dtype=np.float64)
    lna = log(alpha)
    # ~ kappa0=params[0]
    # ~ nu0=params[1]
    mean_im = np.zeros((nbmod), dtype=np.float64)
    for m in range(nbmod):
        s = np.sum(im0[tag != -1, m])
        mean_im[m] = s / np.float64(nmask)
    # ~ print("mask",nmask,"means :",mean_im)
    precalc = np.empty(SIZE_MAX_SEGMENT, dtype=np.float64)
    for l in range(SIZE_MAX_SEGMENT):
        precalc[l] = lgamma(.5 * (nu0 + np.float64(l))
                                 ) - .5 * log(kappa0 + np.float64(l))
        # init sig2 !!!
    for m in range(nbmod):
        if sig2[m] <= 0.:
            sig2[m] = np.var(im0[tag != -1, m])
            updatevar[m] = True
    for i in range(n):
        sum0[i] = 1
        sumx[i] = im0[i] - mean_im
        sumx2[i] = (im0[i] - mean_im) * (im0[i] - mean_im)
        v = i
        successeur[i] = v  # voisins[i][1]
        # ~ precedents[i,0]=v
        precedents_root[i] = v
        precedents_chain[v] = -1
        if tag[i] != -1:
            tag[i] = i
            nbtag += 1
#     main loop iterations
    index = np.nonzero(tag != -1)[0]  # np.arange(n)
    #~ print('iteration:\t\t nbtag\t\t ind_label\t loglik\t\t best')#
    for iter in range(nbiter):
        shuffle(index)
        nu0sig02 = nu0 * sig2
        # logZNIGv(kappa0,.5*nu0,nu0sig02)#np.sum(logZNIG(kappa0,.5*nu0,nu0sig02))#
        p00 = logZNIGm(precalc[0], .5 * nu0, nu0sig02)
        #~ print(iter,"\t\t\t",nbtag,"\t\t",ind_label,"\t",sig2)#
        for i in range(nmask):
            ind = index[i]
            # ~ if(tag[ind]<0):
            # ~ continue
#             get ind neighbours
            # ~ candidats=voisins[ind][voisins[ind]>=0]
            if neighbors_in_time:
                candidats = my_voisins(ind, dims, cnx, tag)
            else:
                candidats = neighbors[index_neighbors[ind]
                    :index_neighbors[ind + 1]]
            nb_candidats = candidats.shape[0]
#             remove link (ind)
            suc = successeur[ind]
            remove_link(ind, suc, precedents_root, precedents_chain)
#             get old/new tag

#             find sub-segment
            # ~ count0=split_segment2(ind,precedents,listind0)
            count0 = split_segment3(
                ind, precedents_root, precedents_chain, listind0)
            listind = listind0[:count0]
#             update tags
            last_tag = tag[ind]
            # ~ newtag=labels_libres[ind_label]
            # ~ labels_libres[ind_label]=-1
            # ~ ind_label-=1
            # ~ nbtag+=1
            if count0 == sum0[last_tag]:
                # ~ ind_label+=1
                # ~ labels_libres[ind_label]=last_tag
                # ~ nbtag-=1
                newtag = last_tag
            else:
                newtag = labels_libres[ind_label]
                labels_libres[ind_label] = -1
                ind_label -= 1
                nbtag += 1
            tag[listind] = newtag
            tag_oldsuc = tag[suc]
#             update stats
            sum0[last_tag] -= count0
            sum0[newtag] = count0
            lval = im0[listind]
            for m in range(nbmod):
                lv = lval[:, m]
                cm = np.float64(count0) * mean_im[m]
                countx[m] = np.sum(lv) - cm
                countx2[m] = np.sum(lv**2) - (cm + 2 * countx[m]) * mean_im[m]
#               MAJ stats
                sumx[last_tag, m] -= countx[m]
                sumx[newtag, m] = countx[m]
                sumx2[last_tag, m] -= countx2[m]
                sumx2[newtag, m] = countx2[m]
            dejap0 = False
            p0 = 0.
            # CALCUL TABLEAU PROBA
            lnproba = lna * np.ones(nb_candidats, dtype=np.float64)
            ll0 = 0.
            for j in range(1, nb_candidats):
                tagj = tag[candidats[j]]
                if tagj != newtag:
                    if (count0 + sum0[tagj]) >= SIZE_MAX_SEGMENT:
                        lnproba[j] = -1e20  # LIMITE LA TAILLE DES SEGMENTS
                        continue
                    dejacalcule = False
                    for jj in range(1, j):
                        if tagj == tag[candidats[jj]]:
                            lnproba[j] = lnproba[jj]
                            dejacalcule = True
                            break
                    if dejacalcule:
                        continue
                    if not dejap0:
                        na = np.float64(count0)
                        kappa_a = kappa0 + na
                        ra = 0.
                        for m in range(nbmod):
                            ra += log(nu0sig02[m] +
                                           countx2[m] - countx[m]**2 / kappa_a)
                        # logZNIGv(kappa_a,.5*(nu0+na),nu0sig02+va-sa**2/kappa_a)#
                        p0 = p00 - nbmod * \
                            precalc[np.int32(na)] + .5 * (nu0 + na) * ra
                        dejap0 = True
#            marginal likelihood mean and variance
                    nb = np.float64(sum0[tagj])
                    kappa_a_b = kappa0 + na + nb
                    kappa_b = kappa0 + nb
                    rb = 0.
                    rab = 0.
                    for m in range(nbmod):
                        rb += log(nu0sig02[m] + sumx2[tagj,
                                                           m] - sumx[tagj, m]**2 / kappa_b)
                        rab += log(nu0sig02[m] + sumx2[tagj, m] + countx2[m] - (
                            sumx[tagj, m] + countx[m])**2 / kappa_a_b)
                    lnproba[j] = p0 + nbmod * (precalc[np.int32(na + nb)] - precalc[np.int32(
                        nb)]) - .5 * ((nu0 + na + nb) * rab - (nu0 + nb) * rb)
                    if tagj == tag_oldsuc:  # tagj==last_tag or
                        ll0 = lnproba[j]
                    # ~ if math.isnan(lnproba[j]):
                        # ~ print("proba nan")
#             sampling
            proba = np.exp(lnproba - lnproba.max())
            ir = randint_choice(nb_candidats, proba)
#             update link
            new_successeur = candidats[ir]
            successeur[ind] = new_successeur
            add_link(ind, new_successeur, precedents_root, precedents_chain)
#            update tags and stats
            tag_successeur = tag[new_successeur]
            if tag_successeur != newtag:  # FUSION DU SEGMENT
                sum0[tag_successeur] += sum0[newtag]
                for m in range(nbmod):
                    sumx[tag_successeur, m] += sumx[newtag, m]
                    sumx2[tag_successeur, m] += sumx2[newtag, m]
                tag[listind] = tag_successeur
                ind_label += 1  # UN LABEL DE LIBRE
                labels_libres[ind_label] = newtag
                # tags
                nbtag -= 1
            if tag_oldsuc != newtag:
                llik -= ll0
            # ~ else:
                # ~ llik-=lna
            if tag_successeur != newtag:
                llik += lnproba[ir]
            # ~ else:
                # ~ llik+=lna
#            update variance
        # for m in range(nbmod):
        #     if False:#updatevar[m]:
        #         moy = sumx[tag[tag != -1], m] / \
        #             sum0[tag[tag != -1]].astype(np.float64)
        #         sig2[m] = np.var(im0[tag != -1, m] - moy)
        print('iter:',iter,'nbseg: ',nbtag,'llik:',llik)
        if (iter>=burn) and ((iter-burn) % decim == 0):
            tagr[:] = tag[:]
            reorder_segments(tagr)
            ret[(iter-burn)//decim,:] = tagr[:]
        # iter
    return ret  

    ###############################################################################
