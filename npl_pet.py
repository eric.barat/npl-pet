#!/usr/bin/python
# -*- coding: utf-8 -*-
#%%
# Load libraries
import os
from pathlib import Path
import numpy as np
os.environ['QT_QPA_PLATFORM']='xcb'
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
from npl_pet_lib import *
from segmentation_lib import get_neighbors, my_boundaries
#%%
# scanner geometry
R = 400
nc = 512
thet = 2.*np.pi/nc*np.arange(nc)
pc = R*np.array([np.cos(thet), np.sin(thet)]).T
# define recon space
dims = np.array((256, 256), dtype = np.int32)
px = 1.
delta = np.ones(2, dtype = np.float64)*px
ori = -.5*delta*dims
x = np.linspace(ori[0], ori[0]+(dims[0]+1)*px, num = dims[0]+1)
y = np.linspace(ori[1], ori[1]+(dims[1]+1)*px, num = dims[1]+1)
mask = np.zeros(dims, dtype = np.int32).ravel()
print('get edge neighbors...')
diagnorm = 2 # use diagonals : 1 to remove
neighbors, idxnb = get_neighbors(np.array(dims, dtype = np.int32), diagnorm, mask)
#%%
# build system matrix
print('system matrix...')
idxsys, lor, system = build_system_matrix(pc, dims, ori, delta)
#%%
# choice of colormap 
cmap = 'viridis'
#%%
# load data
# Phantom
print('load phantom...')
phantom=np.zeros(dims, dtype = np.float64)
lesion=np.zeros(dims, dtype = bool)
# data_path = Path(os.path.dirname(os.path.realpath(__file__))) / 'data'
data_path = Path(os.getcwd()) / 'data'
# Choice of brain slice
slice=71
data=np.fromfile(data_path / "phantom_1.0mm_normal_crisp_181x217x181.rawb", dtype=np.uint8, count=-1).reshape(181,217,181)[slice]#
X,Y=np.meshgrid(np.arange(181),np.arange(217))
print('create lesion')
tumeur=(data==2)*(((X-50)**2+(Y-150)**2)<10**2)
ph = (data==1)*.5+(data==2)*4.+(data==3)*1.+((data==4)+(data==9))*1.+(data==8)*1.+tumeur*2. #Simon
oriph = (dims-ph.shape)//2
phantom[oriph[0]:oriph[0]+ph.shape[0],oriph[1]:oriph[1]+ph.shape[1]]=ph
lesion[oriph[0]:oriph[0]+ph.shape[0],oriph[1]:oriph[1]+ph.shape[1]]=tumeur[:]
# define profile across lesion
xprof = 150
# normalize scale
vm = max(phantom.ravel()/np.sum(phantom))
# Phantom
fig1, ax1 = plt.subplots(nrows=1, ncols=1)
ax1.set_aspect('equal')
ax1.set_xticks([])
ax1.set_yticks([])
ax1.pcolormesh(x, y, phantom/np.sum(phantom), vmin = 0, vmax = vm, cmap = cmap)
fig1.tight_layout()
fig1.savefig(f'theo_phantom.png', bbox_inches='tight', dpi=300)
# Phantom mask
print('compute phantom mask')
mask_hull = np.nonzero(phantom.ravel())[0]
pts = np.moveaxis(np.meshgrid(np.arange(dims[0]),np.arange(dims[1]), indexing='ij'), 0, -1).reshape(-1, 2)
hull = ConvexHull(pts[mask_hull])
nbpts = len(pts)
lpts = np.c_[pts, np.ones(nbpts)].astype(np.float64)
ptsok = np.sum(lpts.dot(hull.equations.T) > 0, 1) == 0
listi = np.zeros(nbpts, dtype=np.int32)
listi[ptsok] = 1
mask = listi.reshape(dims)
# MRI data
raw_anat=np.fromfile(data_path / "t1_icbm_normal_1mm_pn3_rf20_181x217x181.raws", dtype=np.uint16, count=-1).reshape(181,217,181)[slice]#
data_anat=np.ones(dims, dtype = np.float64)*raw_anat[0,0]
data_anat[oriph[0]:oriph[0]+raw_anat.shape[0],oriph[1]:oriph[1]+raw_anat.shape[1]]=raw_anat
# load segmentations array
dict_tags = np.load(data_path / "anat_segmentations.npz")
tags = dict_tags['arr_0']
# choose last segmentation (arbitrary could be : tags[np.random.randint(len(tags))])
tag = tags[-1]*mask.ravel()
# get segmentation boundaries
lines = my_boundaries(tag.reshape(dims), np.arange(dims[1]).astype(np.float64),np.arange(dims[0]).astype(np.float64)).T
# segmented system matrix 
segment_frame = build_segment_frame(tags[-1,:], mask.ravel())
print('segments',len(segment_frame))
idxsys_z, lor_z, system_z, idxframe, lorframe, sysframe = frame_system_matrix(idxsys, lor, system, segment_frame)
# condition number of segmented system matrix
singular_values = system_condition_number(idxsys_z, lor_z, system_z)
condition_number = max(singular_values)/min(singular_values)#
print(f"condition number = {condition_number:.3e}")
# MRI + segments boudaries
fig1, ax1 = plt.subplots(nrows=1, ncols=1)
ax1.pcolormesh(x, y, data_anat, vmin = 0, vmax = max(data_anat.ravel()), cmap = 'gray')
ax1.set_aspect('equal')
ax1.set_xticks([])
ax1.set_yticks([])
bounds, = ax1.plot(ori[0]+lines[0]*px, ori[1]+lines[1]*px, color='yellow', lw=0.5, alpha=1)#'crimson'
ax1.set_xlim([x[0],x[-1]])
ax1.set_ylim([y[0],y[-1]])
fig1.tight_layout()
fig1.savefig('mri_segment_figure.png', bbox_inches='tight', dpi=300)
###########################################
#%%
# Generate synthetic data
print('synthetic data generation...')
Lamb0 = 5.e5 # reference intensity for t=1.
t = 1. # accumulation time 
T = Lamb0*t
hsino = generate_data_sys(phantom.ravel(), idxsys, lor, system, Lamb0*t)
sino_ind = np.nonzero(hsino)[0].astype(np.int32)
sino_counts = hsino[sino_ind].astype(np.float64)
# plot data sinogram
fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize = (6,6))
phys_sino, phi_sino, delta_sino = coord_physical_lor(pc, hsino)#
ax1.pcolormesh(delta_sino, phi_sino, phys_sino, cmap = cmap)#, 
ax1.set_title('Data sinogram')
ax1.set_xlim([-nc//4,nc//4])
ax1.set_xlabel('Shift')
ax1.set_ylabel('Angle')
fig1.tight_layout()
fig1.savefig(f'data_sinogram_t={t:.0f}.png', bbox_inches='tight', dpi=300)
#
#%% 
# MAP COSEM Parameters
#penalty
beta = 2e-3#1e-3#4.e-3
nu = 0.15#.25
eta = 20.#10.
beta_phantom = 1e-3
L = 100 # number of iterations
S = 4 # number of subsets
#
#%%
# "Achievable" phantom : map-cosem of projected phantom
gamma_phantom = segment_project(idxsys, lor, system, phantom.ravel()) 
gamma_phantom *= Lamb0/np.sum(gamma_phantom)
# print('sum gamma_phantom',np.sum(gamma_phantom))
sino_ind_phantom = np.nonzero(gamma_phantom)[0].astype(np.int32)
sino_counts_phantom = gamma_phantom[sino_ind_phantom].astype(np.float64)
print("Achievable phantom from reconstruction of projected theoretical phantom")
verbose = True
lambda_phantom = map_cosem_tomo(idxsys, lor, system, sino_ind_phantom, sino_counts_phantom, idxnb, neighbors, 
np.empty(0,dtype=np.float64), beta_phantom, eta, nu, S, L, verbose).reshape(dims)
fig1, ax1 = plt.subplots(nrows=1, ncols=1)
ax1.set_aspect('equal')
ax1.set_xticks([])
ax1.set_yticks([])
ax1.pcolormesh(x, y, lambda_phantom/Lamb0, vmin = 0, vmax = vm, cmap = cmap)
fig1.tight_layout()
fig1.savefig(f'map_cosem_phantom_figure_beta={beta_phantom:.2e}.png', bbox_inches='tight', dpi=300)
# Profile phantom vs achievable 
fig2, ax2 = plt.subplots(nrows=1, ncols=1)
ax2.plot(.5*(x[1:]+x[:-1]),np.sum(lambda_phantom[oriph[0]+xprof:oriph[0]+xprof+1,:],axis=0)/Lamb0, label = 'Recon. phantom')#
ax2.plot(.5*(x[1:]+x[:-1]),np.sum(phantom[oriph[0]+xprof:oriph[0]+xprof+1,:],axis=0)/np.float64(np.sum(phantom)),alpha=.67,label = 'Phantom', linestyle='--')
ax2.set_xlim([x[0],x[-1]])
ax2.set_ylim([0.,1.6e-4])
fig2.savefig(f'profile_phantom_figure_beta={beta_phantom:.2e}.pdf', bbox_inches='tight')
#%%
# MAP cosem recon
print('MAP-COSEM recon...')
verbose = True
lambda_mapcosem = map_cosem_tomo(idxsys, lor, system, sino_ind, sino_counts/t, idxnb, neighbors,
np.empty(0,dtype=np.float64), beta, eta, nu, S, L, verbose).reshape(dims)
fig1, ax1 = plt.subplots(nrows=1, ncols=1)
ax1.set_aspect('equal')
ax1.set_xticks([])
ax1.set_yticks([])
ax1.pcolormesh(x, y, lambda_mapcosem/Lamb0, vmin = 0, vmax = vm, cmap = cmap)
fig1.tight_layout()
fig1.savefig(f'map_cosem_figure_beta={beta:.2e}.png', bbox_inches='tight', dpi=300)
#%%
# Nonparametric posterior learning
print('NPL-COSEM recon...')
# fraction parameter (rho)
rho =1.# 0.5#0.25#2.#
# number of bootstrap samples
B = 64
#
mu_ini = np.empty(0, dtype = np.float64)
lambda_ini = np.empty(0, dtype = np.float64)
verbose = False
t0 = time.time()
lambda_npl = npl_map_cosem_anat_tomo(idxsys, lor, system, hsino, idxnb, neighbors, beta, eta, nu, S, L, B, t,
    rho, idxsys_z, lor_z, system_z, mu_ini, lambda_ini, verbose)
t1 = time.time()
print('duration', t1-t0)
print("Compute mean and std...")
lambda_mean_npl = np.mean(lambda_npl,axis=0).reshape(dims)
lambda_std_npl = np.std(lambda_npl, axis=0).reshape(dims)
# 
print("Compute credible bands...")
trace_lambda = np.moveaxis(lambda_npl, 0, -1).reshape(dims[0],dims[1],-1)
trace_lesion = np.array([np.mean(trace_lambda[:,:,b][lesion]) for b in range(B)])
# sort_lesion = np.sort(trace_lesion)
# print('mean/std lesion npl :',np.mean(trace_lesion)/Lamb0,np.std(trace_lesion)/Lamb0)
# print('lesion CI :',sort_lesion[int(.025*B)]/Lamb0,sort_lesion[int(.975*B)]/Lamb0)
sort_npl = np.sort(trace_lambda)
minCI = sort_npl[:,:,int(.025*B)]
maxCI = sort_npl[:,:,int(.975*B)]
fig1, ax1 = plt.subplots(nrows=1, ncols=1)
ax1.set_aspect('equal')
ax1.set_xticks([])
ax1.set_yticks([])
ax1.pcolormesh(x, y, lambda_mean_npl/Lamb0, vmin = 0, vmax = vm, cmap = cmap)
fig1.tight_layout()
fig1.savefig(f'mean_npl_figure_t={t:.0f}_rho={rho:.2f}_beta={beta:.2e}.png', bbox_inches='tight', dpi=300)
#
fig1, ax1 = plt.subplots(nrows=1, ncols=1)
gnpb,=ax1.plot(.5*(x[1:]+x[:-1]),np.sum(lambda_mean_npl[oriph[0]+xprof:oriph[0]+xprof+1,:],axis=0)/Lamb0, alpha = 1, label = 'Post. mean')#
ax1.fill_between(.5*(x[1:]+x[:-1]),minCI[oriph[0]+xprof]/Lamb0,maxCI[oriph[0]+xprof]/Lamb0,color = gnpb.get_color(),alpha=.25, label = r'''Post. 95% intervals''')
ax1.plot(.5*(x[1:]+x[:-1]),np.sum(lambda_phantom[oriph[0]+xprof:oriph[0]+xprof+1,:],axis=0)/Lamb0, linestyle = '--', label = 'Lambda phantom')
ax1.set_xlim([x[0],x[-1]])
ax1.set_ylim([0.,1.6e-4])
ax1.set_yticks([])
fig1.tight_layout()
fig1.savefig(f'profile_mean_npl_figure_t={t:.0f}_rho={rho:.2f}_beta={beta:.2e}.pdf', bbox_inches='tight')
#
fig1, ax1 = plt.subplots(nrows=1, ncols=1)
ax1.set_aspect('equal')
ax1.set_xticks([])
ax1.set_yticks([])
ax1.pcolormesh(x, y, 3.*lambda_std_npl/Lamb0, vmin = 0, vmax = vm, cmap = cmap)
fig1.tight_layout()
fig1.savefig(f'std_npl_figure_t={t:.0f}_rho={rho:.2f}_beta={beta:.2e}.png', bbox_inches='tight', dpi=300)
#
fig1, ax1 = plt.subplots(nrows=1, ncols=1)
ax1.set_aspect('equal')
ax1.set_xticks([])
ax1.set_yticks([])
ax1.pcolormesh(x, y, lambda_phantom/Lamb0, vmin = 0, vmax = vm, cmap = 'gray', alpha=.85, zorder = 2)
fail_CI = np.where(lambda_phantom<minCI-1e-16*Lamb0,0,np.where(lambda_phantom>maxCI, 1, np.nan))
ax1.pcolormesh(x, y, fail_CI, vmin = 0, vmax = 1, cmap = 'bwr', alpha=.85, zorder = 2.5)
fig1.tight_layout()
fig1.savefig(f'fail_CI_figure_t={t:.0f}_rho={rho:.2f}_beta={beta:.2e}.png', bbox_inches='tight', dpi=300)
#